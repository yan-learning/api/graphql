var { buildSchema, graphql } = require('graphql')
var express = require("express")
var { graphqlHTTP } = require("express-graphql")
var fs = require('fs').promises;

const PORT = 3000;

var schema = buildSchema(`
input MessageInput {
    content: String
    author: String
}

type Message {
    id: ID!
    content: String
    author: String
}

type Person{
    id: String
    firstname: String
    lastname: String
    age: Int
}

type Mutation {
    createMessage(input: MessageInput): Message
    updateMessage(id: ID!, input: MessageInput): Message
}

type RandomDie {
    numSides: Int!
    rollOnce: Int!
    roll(numRolls: Int!): [Int]
}

type Query {
    getPerson(id: String): Person
    sayHi(name:String): String
    hello: String
    quoteOfTheDay: String
    random: Float!
    rollDice(numDice: Int!, numSides: Int): [Int] 
    getDie(numSides: Int): RandomDie
    getMessage(id: String!): Message

}`)

class Person {
    constructor(id, {firstname, lastname, age}){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }
}

var fakeDatabase = {}

class Message {
    constructor(id, {content, author}){
        this.id = id
        this.content = content
        this.author = author
    }
}

class RandomDie {

    constructor(numSides) {
        this.numSides = numSides
    }

    rollOnce() {
        return 1 + Math.floor(Math.random() * this.numSides)
      }
    
    roll({ numRolls }) {
    var output = []
    for (var i = 0; i < numRolls; i++) {
        output.push(this.rollOnce())
    }
    return output
    }

}

var rootValue = {

    getPerson:({id})=>{

        return new Person(id, {firstname:"Tom", lastname:"Li", "age":10});
    },
    sayHi:({name}) =>{
        return `Hi ${name} nice to meet you`;
    },
    setMessage: ({message}) => {
        fakeDatabase.message = message;
        return message
    },
    getMessage: ({id})=>{
        if (!fakeDatabase[id]) {
            throw new Error("no message exists with id " + id)
          }
          return new Message(id, fakeDatabase[id])
    },
    createMessage: ({input})=>{
        var id = require("crypto").randomBytes(10).toString("hex")
        fakeDatabase[id] = input
        var response = new Message(id, input);
        //console.log(response, response.id);
        return response;
    },
    updateMessage: ({id, input})=>{
        if (!fakeDatabase[id]) {
            throw new Error("no message exists with id " + id)
          }
          // This replaces all old data, but some apps might want partial update.
          fakeDatabase[id] = input
          return new Message(id, input)
    },
    hello: () => {
        return "Hello world"
    },
    quoteOfTheDay: () => {
        return Math.random() < 0.5 ? "Take it easy" : "Salvation lies within"
    },
    random: () => {
        return Math.random()
    },
    rollDice: (args) => {
        var output = [];
        for(var i =0; i< args.numDice; i++){
            output.push(1+ Math.floor(Math.random() * args.numSides || 6));
        }
        //return [1, 2, 3].map(_ => 1 + Math.floor(Math.random() * 6))
        return output;
    },
    getDie: ({ numSides }) => {
        return new RandomDie(numSides || 6)
    },  
}

const logginMiddleware = (req, res, next) => {
    console.log(`ip: ${req.ip}`);
    next();
}

var app = express();

app.use(express.static(__dirname));
app.use(logginMiddleware);

app.use('/graphql', 
    graphqlHTTP({
        schema,
        rootValue,
        graphiql: true,
    })
)

app.get("/", async (req, res)=>{
    const content = await fs.readFile("index.html");
    res.readFile("index.html");

})

app.listen(PORT);
console.log(`Running a GraphQL API server at http://localhost:${PORT}/graphql`);
