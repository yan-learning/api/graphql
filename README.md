# Background

1. Following https://graphql.org/graphql-js/ to do the tutorial


# Installation

## Package 

* express
* express-graphql
* graphql

1. Install packages
   ```
   # Install packages
   npm i
   # Start server
   node server.js
   ```
1. access http://127.0.0.1:3000 in browser and the request will load the content in index.html
1. index.html will do two jobs, one is send a request to server by graphql for creating a message
1. secondly will send a get message for querying the last message by id   
